use std::f64::consts;

use ambassador::{delegatable_trait, Delegate};
use serde::{Serialize, Deserialize};


#[delegatable_trait]
pub trait At {
    fn at(&mut self, center: [f64; 2]) -> [f64; 2];
}


#[derive(Delegate, Serialize, Deserialize, Clone, Debug)]
#[delegate(At)]
pub enum Gear {
    Circle(Circle),
    Oblong(Oblong),
}

impl From<Circle> for Gear {
    fn from(value: Circle) -> Self {
        Self::Circle(value)
    }
}

impl From<Oblong> for Gear {
    fn from(value: Oblong) -> Self {
        Self::Oblong(value)
    }
}


#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Circle {
    angle: f64,
    radius: f64,
    rate: f64,
}

impl Circle {
    pub fn new(radius: f64, rate: f64, resolution: f64) -> Self {
        let rate = rate * 2.0 * consts::PI / resolution;

        Self {
            angle: 0.0,
            radius,
            rate,
        }
    }

    pub fn angle(mut self, value: f64) -> Self {
        self.angle = value.to_radians();
        self
    }

    pub fn update(&mut self) {
        self.angle += self.rate;
    }
}

impl At for Circle {
    fn at(&mut self, center: [f64; 2]) -> [f64; 2] {
        let tip_x = center[0] + self.radius * self.angle.sin();
        let tip_y = center[1] + self.radius * self.angle.cos();

        self.update();

        return [tip_x, tip_y];
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Oblong {
    angle: f64,
    radius: f64,
    rate: f64,
    oblong_rate: f64,
}

impl Oblong {
    pub fn new(radius: f64, rate: f64, oblong_rate: f64, resolution: f64) -> Self {
        let rate = rate * 2.0 * consts::PI / resolution;

        Self {
            angle: 0.0,
            radius,
            rate,
            oblong_rate,
        }
    }

    pub fn angle(mut self, value: f64) -> Self {
        self.angle = value.to_radians();
        self
    }

    pub fn update(&mut self) {
        self.angle += self.rate;
    }
}

impl At for Oblong {
    fn at(&mut self, center: [f64; 2]) -> [f64; 2] {
        let radius = self.radius / 2.0 * (self.angle * self.oblong_rate).sin() + self.radius / 2.0;
        let tip_x = center[0] + radius * self.angle.sin();
        let tip_y = center[1] + radius * self.angle.cos();

        self.update();

        return [tip_x, tip_y];
    }
}


pub fn spiro(center: [f64; 2], gears: &mut Vec<Gear>, iterations: u32) -> Vec<[f64;2]> {
    let mut res = Vec::new();

    for _ in 0..iterations {
        let mut point = center;

        for gear in gears.iter_mut() {
            point = gear.at(point);
        }

        res.push(point);
    }

    res
}