use opengl_graphics::{GlyphCache, GlGraphics};
use graphics::{text, DrawState, Context, Transformed, rectangle, CharacterCache};
use rusttype::Scale;

use crate::colors::WHITE;

pub fn font_height(glyph_cache: &GlyphCache, size: u32) -> f64 {
    let scale = Scale::uniform(((size as f32)* 1.333).round());
    let v = glyph_cache.font.v_metrics(scale);
    (v.ascent - v.descent) as f64
}

pub fn render_options(ctx: &Context, gl: &mut GlGraphics, glyph_cache: &mut GlyphCache, size: u32, options: Vec<&str>, selected: u32) {
    let ds = DrawState::new_alpha();
    let height = font_height(glyph_cache, size);
    let max_width = options.iter().map(|x| glyph_cache.width(size, &x).unwrap()).reduce(f64::max).unwrap_or_default();

    let text_t = ctx.transform.zoom(0.5);

    let top = height * (selected as f64);
    let left = 5.0;
    let rect = rectangle::rectangle_by_corners(left, top, left + max_width, top + height);
    rectangle([0.4, 0.2, 0.6, 1.0], rect, text_t, gl);

    let txt = text::Text::new_color(WHITE, size);
    let mut y = height as f64;
    for line in options {
        txt.draw(&line, glyph_cache, &ds, text_t.clone().trans(left, y), gl).unwrap();
        y += height;
    }
}