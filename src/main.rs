#![windows_subsystem = "windows"]

use std::path::{Path, PathBuf};

use opengl_graphics::{GlGraphics, OpenGL, GlyphCache, TextureSettings};
use glutin_window::GlutinWindow;
use piston::{UpdateArgs, EventSettings, WindowSettings, EventLoop, Events, RenderEvent, UpdateEvent, RenderArgs, ReleaseEvent, Button, Key};
use graphics::{types::Matrix2d, DrawState};

use rand::Rng;

mod gear;
mod colors;
mod io;
mod menu;
use gear::*;
use colors::*;
use io::*;
use menu::*;


pub struct App<'a, 'b> {
    gl: GlGraphics,
    i: u32,
    limit: Option<u32>,
    pause: bool,
    color_i: usize,
    cmap: &'a Cmap,
    gears: Vec<Gear>,
    glyphs: GlyphCache<'b>,
    selected: usize,

    display_list: Option<Vec<String>>,
}


fn random_spiro_dec(resolution: f64) -> Vec<Gear> {
    let mut rng = rand::thread_rng();
    let mut gears = Vec::new();
    let mut max: f64 = 230.0;
    for _ in 0..rng.gen_range(2..=5) {
        let radius = rng.gen_range(10.0..max);
        max = radius.max(20.0);
        let rate = rng.gen_range(0.1..4.0);
        gears.push(Gear::Circle(Circle::new(radius, rate, resolution)))
    }

    gears
}


fn random_spiro(resolution: f64) -> Vec<Gear> {
    let mut rng = rand::thread_rng();
    let mut gears = Vec::new();
    for _ in 0..rng.gen_range(2..=5) {
        let radius = rng.gen_range(10.0..230.0);
        let rate = rng.gen_range(0.1..4.0);
        gears.push(Gear::Circle(Circle::new(radius, rate, resolution)))
    }

    gears
}


impl<'a, 'b> App<'a, 'b> {
    fn setup(&mut self) {
        //t = t.rot_deg((self.i / 2) as f64);
        //let mut gears = vec![
        //    Circler::new(150.0, 0.5, 80.0),
        //    Circler::new( 20.0, 2.7, 80.0),
        //    Circler::new( 40.0, 1.1, 80.0),
        //];
        //let mut gears = vec![
        //    Circler::new(250.0, 0.5, 80.0),
        //    Circler::new( 40.0, 2.1, 80.0).angle(10.0),
        //    Circler::new( 40.0, 2.2, 80.0).angle(10.0),
        //    Circler::new( 40.0, 2.3, 80.0).angle(10.0),
        //];
        /*
        let r = 200.0;
        let mut c1 = Circle::new(200.0, 0.20, r);
        let mut c2 = Circle::new(100.0, 5.00, r).angle(40.0);
        let mut o1 = Oblong::new(150.0, 1.50, 8.0, r).angle(40.0);
        let mut gears: Vec<&mut dyn Circler> = vec![&mut c1, &mut c2, &mut o1];
        */

        /*
        let r = 200.0;
        let mut c1 = Circle::new(200.0, 0.20, r);
        let mut o1 = Oblong::new(150.0, 1.50, 4.0, r).angle(40.0);
        let mut gears: Vec<&mut dyn Circler> = vec![&mut c1, &mut o1];
        */

        // Good logo??
        let r = 200.0;
        self.gears = vec![
            Gear::Circle(Circle::new(200.0, 0.20, r)),
            Gear::Oblong(Oblong::new(150.0, 0.50, 0.5, r).angle(40.0)),
        ];

        /*
        let r = 200.0;
        let mut c1 = Circle::new(200.0, 2.17, r);
        let mut c2 = Circle::new(100.0, 0.634, r).angle(30.0);
        let mut gears: Vec<&mut dyn Circler> = vec![&mut c1, &mut c2];
        */

        // Sydney demo
        /*
        let r = 200.0;
        self.gears = vec![
            Circle::new(300.0, 0.8, r),
            Circle::new(50.0, 7.00, r),
            Circle::new(250.0, 1.00, r),
            Circle::new(10.0, 4.00, r),
        ].into_iter().map(|x| x.into()).collect();
        */
    }

    fn render(&mut self, args: &RenderArgs) {
        self.gl.draw(args.viewport(), |ctx, gl| {
            graphics::clear([0.0, 0.0, 0.0, 0.0], gl);
            use graphics::*;

            let [w, h] = ctx.get_view_size();
            let t = ctx.transform.trans(w / 2.0, h / 2.0);

            let points = gear::spiro([0.0, 0.0], &mut self.gears.clone(), self.i);
            if Self::check_returned(&points) {
                self.pause = true;
            }

            Self::draw_lines(points, self.cmap, t, gl);

            if let Some(ref spiros) = self.display_list {
                render_options(&ctx, gl, &mut self.glyphs, 24, spiros.iter().map(|x| x.as_str()).collect(), self.selected as u32);
            }
        })
    }

    fn update(&mut self, _args: &UpdateArgs) {
        if !self.pause {
            self.i += 1;
        }

        if self.limit.map_or(false, |x| self.i > x) {
            self.i = 0;
        }
    }

    fn draw_lines(points: Vec<[f64; 2]>, cmap: &Cmap, transform: Matrix2d, gl: &mut GlGraphics) {
        let ds = DrawState::new_alpha();

        for (i, p) in points.iter().enumerate().skip(1) {
            let c = map_value(i as f64, 0.0, (points.len() - 1) as f64, cmap);
            let l = graphics::Line::new(exalpha(c, 1.0), 1.0);
            l.draw_from_to(points[i-1], *p, &ds, transform, gl);
        }
    }

    fn check_returned(points: &Vec<[f64; 2]>) -> bool{
        if points.len() > 1 {
            let s = points.first().unwrap();
            let e = points.last().unwrap();
            if (s[0] - e[0]).abs() < 1.0e-9 && (s[1] - e[1]).abs() < 1.0e-9 {
                return true;
            }
        }

        return false;
    }

    fn next_color(&mut self) {
        self.color_i = (self.color_i + 1) % COLOR_MAPS.len();
        self.cmap = &COLOR_MAPS[self.color_i];
    }

    fn save_gears(&self) {
        save_spiro(&self.gears);
    }

    fn toggle_infinite(&mut self) {
        self.limit = match self.limit {
            Some(_) => None,
            None => Some(5000),
        };
    }

    fn random_spiro(&mut self) {
        self.i = 0;
        self.gears = random_spiro_dec(400.0);
    }

    fn up(&mut self) {
        self.selected = self.selected.saturating_sub(1)
    }

    fn down(&mut self) {
        if let Some(ref spiros) = self.display_list {
            let mut max = spiros.len();
            if max > 0 {
                max -= 1;
            }
            self.selected = max.min(self.selected + 1);
        }
    }

    fn list_spiros(&mut self) {
        self.display_list = match self.display_list {
            Some(_) => {
                self.selected = 0;
                None
            },
            None => Some(list_spiros()),
        };
    }

    fn load_spiro(&mut self) {
        if let Some(ref spiros) = self.display_list {
            self.gears = load_spiro(spiros.get(self.selected as usize).unwrap());
            self.i = 0;
        }
    }
}


fn main() {
    let opengl = OpenGL::V3_2;

    let mut window: GlutinWindow = WindowSettings::new("Spiros", [700, 600])
                             .graphics_api(opengl)
                             .samples(8)
                             .resizable(true)
                             .exit_on_esc(true)
                             .build()
                             .unwrap();

    let font_filename = "Roboto-Light.ttf";
    let exe_dir = std::env::current_exe().expect("Bad current executable path fetch.");
    let mut font_path = exe_dir.parent().map(|p| Path::join(p, font_filename))
                               .expect("Unable to construct font path.");
    if !font_path.exists() {
        font_path = PathBuf::from(font_filename);
    }

    let glyphs = GlyphCache::new(font_path, (), TextureSettings::new())
                 .expect(&format!("Unable to load font: {}", font_filename));

    let mut app = App {
        gl: GlGraphics::new(opengl),
        i: 0,
        limit: Some(5000),
        pause: false,
        color_i: 0,
        cmap: &COLOR_MAPS[0],
        gears: vec![],
        glyphs: glyphs,
        selected: 0,
        display_list: None,
    };

    app.setup();

    let mut events = Events::new(EventSettings::new().max_fps(60));
    while let Some(e) = events.next(&mut window) {
        if let Some(args) = e.render_args() {
            app.render(&args);
        }

        if let Some(args) = e.update_args() {
            app.update(&args);
        }

        if let Some(Button::Keyboard(key)) = e.release_args() {
            match key {
                Key::Space => app.pause = !app.pause,
                Key::Return => app.load_spiro(),
                Key::Up   => app.up(),
                Key::Down => app.down(),
                Key::C  => app.next_color(),
                Key::S  => app.save_gears(),
                Key::R  => app.random_spiro(),
                Key::I  => app.toggle_infinite(),
                Key::L  => app.list_spiros(),
                _ => {},
            };
        }
    }
}
