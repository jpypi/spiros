use std::fs::File;
use std::path::PathBuf;
use std::io::{Write, BufReader};
use regex::Regex;

use crate::gear::*;


pub fn save_spiro(gears: &Vec<Gear>) {
    let serialized = serde_json::to_string(gears).unwrap();

    let mut path = PathBuf::from("saved_spiros");

    // Create save folder
    if let Err(e) = std::fs::create_dir(&path) {
        if e.kind() != std::io::ErrorKind::AlreadyExists {
            println!("Unable to create saved_spiros directory");
            return
        }
    }

    // Determine max number to add to save file to not overwrite previous
    let mut max_i: u32 = 0;
    let re = Regex::new(r"^spiro-(\d+)\.json$").unwrap();
    if let Ok(dir_contents) = path.read_dir() {
        for entry in dir_contents {
            if let Ok(e) = entry {
                let fname = e.file_name();
                if let Some(capture) = re.captures(fname.to_str().unwrap()) {
                    let i: u32 = capture.get(1).unwrap().as_str().parse().unwrap();
                    max_i = max_i.max(i);
                }
            }
        }
    }

    path.push(format!("spiro-{}.json", max_i + 1));

    // Save spiro params
    match File::create(&path) {
        Ok(mut f) => {
            if let Err(e) = f.write_all(serialized.as_bytes()) {
                println!("Unable to write to {}.\n{:?}", path.display(), e);
            }
        },
        Err(e) => println!("Unable to save sprio!\n{:?}", e.kind()),
    };
}


pub fn load_spiro(fname: &str) -> Vec<Gear> {
    let mut path = PathBuf::from("saved_spiros");
    path.push(fname);

    if let Ok(f) = File::open(path) {
        let reader = BufReader::new(f);
        if let Ok(res) = serde_json::from_reader(reader) {
            return res;
        }
    }

    Vec::new()
}


pub fn list_spiros() -> Vec<String> {
    let path = PathBuf::from("saved_spiros");

    let mut spiros = Vec::new();

    // Determine max number to add to save file to not overwrite previous
    let re = Regex::new(r"\.json$").unwrap();
    if let Ok(dir_contents) = path.read_dir() {
        for entry in dir_contents {
            if let Ok(e) = entry {
                let os_fname = e.file_name();
                let fname = os_fname.to_str().unwrap();
                if re.is_match(&fname) {
                    spiros.push(fname.into());
                }
            }
        }
    }

    spiros
}