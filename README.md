# About
[![pipeline status](https://gitlab.com/jpypi/spiros/badges/master/pipeline.svg?key_text=build+(master)&key_width=100)](https://gitlab.com/jpypi/spiros/-/pipelines/latest)

Spiros renders visualizations similar to what you might get from an old-school spirograph set. The fancy part is that you can use weird shapes and have way more than just one gear inside another, and can thus get all sorts of cool shapes. Theoretically, with infinite "spiros", you arrive at a fourier series and can actually render any continuous line shape.


# Usage

key      |functionality
---------|-------------
spacebar |Pause/play animation.
Up       |Change spiro to load from saved list.
Down     |Change spiro to load from saved list.
Enter    |Load selected saved spiro.
l        |Display load menu, listing saved spiros.
c        |Switch colormap used to render spiro.
s        |Save the current spiro to "saved_spiros/spiro-{N}.json".
r        |Generate a new random spiro.
i        |Toggle infinte mode vs limit mode (stops after a 5000 steps and restarts the animation)